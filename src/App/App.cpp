#include <thread>
#include "App/Common.hpp"
#include "Network/Client.hpp"
#include "Network/Server.hpp"
#include "App/App.hpp"
#include "App/Constants.hpp"
#include "App/Log.hpp"


App::App() {
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	LOG::init();
	gpio_init();
	LOG::DEBUG("Running in debug mode! You should't be running this in production!", 0);
	vehicle = std::make_unique<Vehicle>();
	ir = std::make_unique<IRCommunicator>(*vehicle);
	client = std::make_unique<Client>(*vehicle);
	server = std::make_unique<Server>(*vehicle);
}

App::~App() {
	gpio_destroy();
}

int App::run() {
	set_running(true);
	std::thread server_thr([this] {server->run();});
	std::thread client_thr([this] {client->run();});
	std::thread ir_thr([this] {ir->run();});

	while(this->running()) {
		vehicle->read_sensors();
		if(gpio_state(17)) {
			if(vehicle->stopped()) {
				LOG::DEBUG("Detected vehicle starting!");
				vehicle->kickoff();
			} else {
				LOG::DEBUG("Detected vehicle stopping!");
				vehicle->stop();
			}
			std::this_thread::sleep_for(std::chrono::seconds(2));
		}
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}

	ir_thr.join();
	client_thr.join();
	server_thr.join();
	LOG::destroy();
	return 0;
}
