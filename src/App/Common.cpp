#include <cmath>
#include "App/Common.hpp"

char* itoa(int num, char* buffer, int base) {
    int curr = 0;
 
    if (num == 0) {
        // Base case
        buffer[curr++] = '0';
        buffer[curr] = '\0';
        return buffer;
    }
 
    int num_digits = 0;
 
    if (num < 0) {
        if (base == 10) {
            num_digits ++;
            buffer[curr] = '-';
            curr ++;
            // Make it positive and finally add the minus sign
            num *= -1;
        }
        else
            // Unsupported base. Return NULL
            return NULL;
    }
 
    num_digits += (int)floor(log(num) / log(base)) + 1;
 
    // Go through the digits one by one
    // from left to right
    while (curr < num_digits) {
        // Get the base value. For example, 10^2 = 1000, for the third digit
        int base_val = (int) pow(base, num_digits-1-curr);
 
        // Get the numerical value
        int num_val = num / base_val;
 
        char value = num_val + '0';
        buffer[curr] = value;
 
        curr ++;
        num -= base_val * num_val;
    }
    buffer[curr] = '\0';
    return buffer;
}

std::mutex gpio_mutex;
volatile uint32_t *gpio;
int gpio_mem_fd;

void gpio_init() {
    gpio_mem_fd = open("/dev/gpiomem", O_RDWR | O_SYNC);
	if(gpio_mem_fd < 0) LOG::DEBUG("Can't read GPIO for key presses!");
	gpio = (uint32_t *) mmap(
		0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, gpio_mem_fd, 0x20200000
	);
	if ((int32_t)gpio < 0) LOG::DEBUG("Can't mmap!");
}

void gpio_destroy() {
    close(gpio_mem_fd);
}

bool gpio_state(int input_pin) { //TODO: rewrite
    volatile long state = gpio[13]&(1<<input_pin);
    return !state;
}