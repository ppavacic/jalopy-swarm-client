#include <stdio.h>      
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <string.h> 
#include <arpa/inet.h>
#include "Vehicle/Vehicle.hpp"

Vehicle::Vehicle() {
    update_my_ip();
};

std::string Vehicle::get_his_ip() const { 
    std::lock_guard<std::mutex> set_ip_guard(his_ip_mutex);
    return his_ip; 
};

bool Vehicle::his_ip_set() const {
    std::lock_guard<std::mutex> get_ip_guard(his_ip_mutex);
    return his_ip.compare("") == 0 ? false : true;
}

void Vehicle::set_his_ip(const std::string new_ip) { 
    std::lock_guard<std::mutex> set_ip_guard(his_ip_mutex);
    if(his_ip.compare(get_my_ip()) == 0) {
        LOG::DEBUG("Self interference detected! Received own IP!", DEBUG::TYPE_IR | DEBUG::LEVEL_BASIC);
        return;
    }
    his_ip = new_ip;
    LOG::DEBUG("His IP set to: " + new_ip, DEBUG::LEVEL_VERBOSE | DEBUG::TYPE_IR);
};

//https://stackoverflow.com/questions/212528/get-the-ip-address-of-the-machine
void Vehicle::update_my_ip() {
    //TODO multi reader single writer lock
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) {
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            if(strcmp(ifa->ifa_name, "wlan0") == 0) {
                my_ip = std::string(addressBuffer);
                printf("%s interface selected. IP Address %s\n", ifa->ifa_name, addressBuffer);
            }
        } /*else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
            // is a valid IP6 Address
            tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
            char addressBuffer[INET6_ADDRSTRLEN];
            inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
            printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer); 
            my_ip = std::string(addressBuffer);
            is_ipv6 = true;
        } */
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);
}