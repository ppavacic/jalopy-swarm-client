#include <iostream>
#include "Test/SimpleTests.hpp"
#include "App/App.hpp"

int main(int argc, char *argv[]) {
	
	if(argc > 1) {
		if(strcmp(argv[1], "test") == 0) test(argc, argv);
		else std::cout << "Unknown command, use \"test\" for testing";
	} else {
		App app;
		app.run();	
	}
}
