#include <arpa/inet.h>
#include <sstream>
#include <thread>
#include <chrono>
#include "App/Log.hpp"
#include "Network/Client.hpp"
#include "Network/Conversation.hpp"
#include "App/Constants.hpp"

Client::Client(Vehicle &vehicle) : vehicle(vehicle), lookup(LOOKUP_IP) {
	//my address
	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET_VER;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(SERV_PORT);
	//setup_socket();
}

Client::~Client() {
	if(serv_fd > 0) close(serv_fd);
}

void Client::setup_socket() {
	serv_fd = socket(AF_INET_VER, SOCK_STREAM, 0);
	tv.tv_sec = MSG_RECV_TIMEOUT_SEC;
	tv.tv_usec = MSG_RECV_TIMEOUT_MSEC;
	setsockopt(serv_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
}

void Client::handle_connection() {
	std::string in_msg;
	Conversation conv(serv_fd, true);
	kickoff::Motion motion;
	conv.cli_init();
	LOG::DEBUG("Opened new Client connection!", DEBUG::TYPE_IP | DEBUG::LEVEL_BASIC);
	while(!conv.is_down()) { //while recv doesn't return invalid, or empty msg
		auto [type, in_msg] = conv.receive();

		switch (type) {
			case kickoff::PING:
				LOG::DEBUG("Received ping from Server!");
			break;
			case kickoff::KICKOFF:
				LOG::DEBUG("Received kickoff signal from Server!");
				vehicle.kickoff();
			break;
			case kickoff::MOTION:
				motion.ParseFromString(in_msg);
				//vehicle.set_swarm_speed(motion.velocity());
				LOG::DEBUG("My motion: " + 
				std::to_string(motion.velocity()) + ", " + 
				std::to_string(motion.acceleration()) + ", " +
				std::to_string(motion.jerk()) + ", ", DEBUG::TYPE_IP | DEBUG::LEVEL_ALL);
			break;
			default:
				std::cout << type << "Unknown message received!" << std::endl;
			break;
		}
		if(!vehicle.his_ip_set()) {
			close(serv_fd); 		//JUST HERE FOR DEMO TODO
			vehicle.stop();			//JUST HERE FOR DEMO
			break;
		} 
		//wait for kickoff signal, ping from time to time
		std::this_thread::sleep_for (std::chrono::milliseconds(WAIT_BETWEEN_MSG_MICS));
	}
	conv.close();
}


void Client::run() {
	set_running(true);
	while(running()) {
		if(vehicle.stopped()) {
			if(vehicle.his_ip_set()) {
				const std::string target_ip = vehicle.get_his_ip();
				LOG::DEBUG("Trying to connect to " + target_ip, DEBUG::LEVEL_BASIC);
				serv_addr.sin_addr.s_addr = Network::ip_text_to_binary(target_ip);
				setup_socket();
				if(connect(serv_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == 0) {
					LOG::DEBUG("Succesfully connected to target!", DEBUG::LEVEL_BASIC);
					handle_connection(); //handle single connection to person in front
				} else {
					LOG::LOG_ERR_NOTIFY("Unable to connect to target!", DEBUG::LEVEL_BASIC);
					std::this_thread::sleep_for(std::chrono::seconds(2));
				} 
			} else {  //if no ip at the moment, try again
				std::this_thread::sleep_for(std::chrono::seconds(2));
				continue;
			}
			
		}
		
	}
}
