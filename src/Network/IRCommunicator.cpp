#include <thread>
#include <iostream>
#include <string.h>
#include <sstream>
#include "App/Constants.hpp"
#include "App/Common.hpp"
#include "Network/irslinger.h"
#include "Network/IRCommunicator.hpp"
#include "Exceptions/IRException.hpp"
#include "Exceptions/TimeoutException.hpp"
#include "Exceptions/UnknownStateException.hpp"

#define PP_FILE "IRCommunicator.cpp"

IRCommunicator::IRCommunicator(Vehicle& vehicle) : vehicle(vehicle) {
	// Initialize GPIO
	gpioInitialise();
	gpioSetMode(IR_READ_PIN, PI_INPUT);

	//calculate binary IP address (used for sending via *transmit* function)
	//TODO check if works with IPV6
	struct sockaddr_in addr;
	inet_aton(vehicle.get_my_ip().c_str(), &addr.sin_addr);

	auto address_b = inet_addr(vehicle.get_my_ip().c_str());
	unsigned char* address_c = (unsigned char *) &address_b;


	for(int i = 0; i < IP_ADDR_SIZE_BITS/8; i++) { //TODO not ipv6 ready
		unsigned char octet_decimal = address_c[i];
		char *octet_binary = &ip_binary_string[i*8];
		for(int j = 7; j >= 0; j--) { 
			octet_binary[j] = octet_decimal%2 + 48;   
			octet_decimal = octet_decimal/2;    
		}
	}
	ip_binary_string[IP_ADDR_SIZE_BITS+1] = '\0';
	LOG::DEBUG("My IP set to " + 
		vehicle.get_my_ip(), DEBUG::LEVEL_BASIC | DEBUG::TYPE_IR);
	LOG::DEBUG("IP transformed to binary form " + 
		std::string(ip_binary_string), DEBUG::LEVEL_ALL | DEBUG::TYPE_IR);

}

IRCommunicator::~IRCommunicator() {
	gpioTerminate();
}

void IRCommunicator::run() {
    start();
	std::thread ir_transmit_thr([this] {	//TODO SEGMENTATION FAULT HERE
		while(transmitting()) {
			transmit_my_ip();
			std::this_thread::sleep_for(std::chrono::milliseconds(IR_THREAD_SLEEP_MS - 100));
		}
	});
    while(receiving()) {
		try {
			receive();
		} catch (TimeoutException &e) {
			if(vehicle.his_ip_set()) vehicle.set_his_ip("");
			else LOG::DEBUG("Received partial IR message!");
		} catch (UnknownStateException &e) {
			if(vehicle.his_ip_set()) vehicle.set_his_ip("");
			else LOG::DEBUG("Received partial IR message!");
		} catch (JalopyException &e) {
			LOG::DEBUG(e.what());
		}

        //std::cout << "Stream: " << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
	ir_transmit_thr.join();
}

std::string IRCommunicator::binary_ip_to_string(const char binary_ip[], int size, int size_per_part) {
	std::string ip_string;
	char binary_ip_string[size +1];
	for(int i = 0; i < size; i++) { //TODO: not ipv6 ready, can't read digits > 9
		binary_ip_string[i] = binary_ip[i] + 48; 
	}
	binary_ip_string[size] = '\0';
	for(int i = 0; i < IP_ADDR_SIZE_BITS/size_per_part; i++) { 
		char binary_octet_ip_string[size_per_part+1];
		binary_octet_ip_string[size_per_part] = '\0';
		memcpy(binary_octet_ip_string, &binary_ip_string[i*size_per_part], size_per_part);
		std::string::size_type size;
		int addr_octet_num = std::stoi(binary_octet_ip_string, &size, 2);
		ip_string += std::to_string(addr_octet_num) + (
			i == IP_ADDR_SIZE_BITS/size_per_part -1 ? "" : IP_PART_SEPARATOR
		);
	}
	return ip_string;
}

void IRCommunicator::transmit_my_ip() {
    uint32_t outPin = 26;            // The Broadcom pin number the signal will be sent on
	int frequency = 38000;           // The frequency of the IR signal in Hz
	double dutyCycle = 0.5;          // The duty cycle of the IR signal. 0.5 means for every cycle,
	                                 // the LED will turn on for half the cycle time, and off the other half
	int leadingPulseDuration = 9000; // The duration of the beginning pulse in microseconds
	int leadingGapDuration = 4500;   // The duration of the gap in microseconds after the leading pulse
	int onePulse = 562;              // The duration of a pulse in microseconds when sending a logical 1
	int zeroPulse = 562;             // The duration of a pulse in microseconds when sending a logical 0
	int oneGap = 1688;               // The duration of the gap in microseconds when sending a logical 1
	int zeroGap = 562;               // The duration of the gap in microseconds when sending a logical 0
	int sendTrailingPulse = 1;       // 1 = Send a trailing pulse with duration equal to "onePulse"
	                                 // 0 = Don't send a trailing pulse
	int result = 0;
	for(int i = 0; i < IR_FRAMES_PER_PACKET; i++) {
		char frame[IR_FRAME_BITS+1] = "00000000000000000000000"; 
		frame[IR_FRAME_BITS] = '\0';
		memcpy(frame, &ip_binary_string[i*IR_FRAME_DATA_BITS], IR_FRAME_DATA_BITS);
		for(int j = 0; j < IR_FRAME_DATA_BITS; j++) {
			frame[IR_FRAME_DATA_BITS +j] = frame[j] == '0' ? '1' : '0';
		}
		result += irSling(
			outPin,
			frequency,
			dutyCycle,
			leadingPulseDuration,
			leadingGapDuration,
			onePulse,
			zeroPulse,
			oneGap,
			zeroGap,
			sendTrailingPulse,
			frame);
		//LOG::DEBUG("Sent frame " + std::string(frame), DEBUG::LEVEL_ALL | DEBUG::TYPE_IR);
		//std::this_thread::sleep_for(std::chrono::milliseconds());
	}
}

std::string IRCommunicator::receive() {
	char ip_address_bits[IR_PACKET_BITS+1];
	char initial_ip_address_bits[IR_PACKET_BITS+1]; //store inital person with whom we were communicating
	bool address_set = false;
	int num_of_transmission_errors = 0;
	start();
	while(is_running()) {
		try {
			listen_for_ip(ip_address_bits);
			if(address_set) { 
				//if address that we are receiving from IR has changed 
				//we don't know what happened, so we are in unknown state
				if(strcmpb(ip_address_bits, (char *) initial_ip_address_bits, IP_ADDR_SIZE_BITS) != 0) 
					throw UnknownStateException(
						"Infrared network detected different IP address", "IRCommunicator.cpp", "receive",
						DEBUG::LEVEL_BASIC | DEBUG::TYPE_IR
				);
			} else { // if address with whom we are communicating is not set, set it %TODO no ipv6 ready
				char str[ADDR_LEN] = {1};
				memcpy(initial_ip_address_bits, ip_address_bits, IP_ADDR_SIZE_BITS);
				//print_binary_data(ip_address_bits, IP_ADDR_SIZE_BITS);
				LOG::DEBUG("Trying to set ip with binary data!", DEBUG::LEVEL_ALL | DEBUG::TYPE_IR);
				vehicle.set_his_ip(
					binary_ip_to_string(initial_ip_address_bits, IP_ADDR_SIZE_BITS, IP_BITS_PER_PART));
				address_set = true;
			}
			num_of_transmission_errors = 0;
		} catch (const JalopyException &e) {
			LOG::DEBUG(e.what(), DEBUG::LEVEL_ALL | DEBUG::TYPE_IR);
			num_of_transmission_errors++;
		}

		if(num_of_transmission_errors > 5)
			throw UnknownStateException("Too many receive errors!", 
			"IRCommunicator.cpp", "receive", DEBUG::LEVEL_BASIC | DEBUG::TYPE_IR);
	}
    return std::string("end");
}

void IRCommunicator::listen_for_ip(char received_binary[IR_PACKET_BITS+1]) {
	int pinState = gpio_state(IR_READ_PIN) == false ? 1 : 0;
	int previousPinState = pinState;
	STATE state = WAIT_HELLO;

	int bit_i = 0;
	char frame[IR_FRAME_BITS] = {1};
	unsigned int last_two_durations[2];
	auto start = std::chrono::system_clock::now();
	for(int frame_i = 0; frame_i < IR_FRAMES_PER_PACKET; ) {
		pinState = gpio_state(IR_READ_PIN) == false ? 1 : 0;
		auto current_time = std::chrono::system_clock::now();
		auto time_diff_mics = std::chrono::duration_cast<std::chrono::microseconds>(current_time - start);

		if(state == RECV_DATA && time_diff_mics.count() >= 2000) {
			throw TimeoutException(
				"Timeout! Partial message received!", 
				PP_FILE, "listen_for_ip", 5
			);
		} else if(state == WAIT_HELLO && time_diff_mics.count()/1000 >= IR_THREAD_SLEEP_MS * 2) {
			if(vehicle.his_ip_set()) LOG::DEBUG("Timeout waiting for IR packet!");
			throw TimeoutException(
				"Timeout! Waiting for HELLO for too long!", 
				PP_FILE, "listen_for_ip", 5
			);
		}

		if(pinState != previousPinState) { //when pin state is changed check duration of last state
			auto end = current_time;
			//std::cout << "STATE: " << state << std::endl;
			last_two_durations[previousPinState] = time_diff_mics.count();
			//std::cout << pinState << " state lasted " << time_diff_mics.count() << " microseconds" << std::endl;
			//classify data by JNEC standard 
			if(pinState == 0) {
				if(state == WAIT_HELLO) {
					if(8700 < last_two_durations[0] && 4000 < last_two_durations[1] && last_two_durations[1] < 4900) 
						state = RECV_DATA;
				} else if (state == RECV_DATA) {
					if(400 < last_two_durations[0] && last_two_durations[0] < 750) {
						if(400 < last_two_durations[1] && last_two_durations[1] < 700) frame[bit_i++] = 0;
						else if(1400 < last_two_durations[1] && last_two_durations[1] < 2000) frame[bit_i++] = 1;
						else throw IRException(
							"Unknown second half cycle, expected 0 or 1!", 
							PP_FILE, "listen_for_ip", 5
						);
						
					} else {
						IRException(
							"Unknown part of signal!", 
							PP_FILE, "listen_for_ip", 5
						);
					}
				}

				//if whole frame has been received
				if(bit_i >= IR_FRAME_BITS) { 
					bit_i = 0;
					state = WAIT_HELLO;
					for(int i = 0; i < IR_FRAME_DATA_BITS; i++) { 
						if(frame[i] != !frame[IR_FRAME_DATA_BITS +i]) { //error check by JNEC standard
							throw IRException(
								"Error check failed, data altered in transmission!", 
								PP_FILE, "listen_for_ip", 5
							);
						}
						received_binary[i + frame_i *IR_FRAME_DATA_BITS] = frame[i];
					}
					//print_binary_data(frame, IR_FRAME_BITS);
					frame_i++;
				} else {
					std::this_thread::sleep_for(std::chrono::microseconds(200));
				}
				
			}
			previousPinState = pinState;
			start = end;
		}
	}
}