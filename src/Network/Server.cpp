#include <thread>
#include <chrono>
#include "App/Log.hpp"
#include "App/Constants.hpp"
#include "Network/Server.hpp"
#include "Network/Conversation.hpp"
#include "Network/Common.hpp"
#include "Protobuf/kickoff.pb.h"

Server::Server(Vehicle &vehicle) : vehicle(vehicle), lookup(LOOKUP_IP) {
	//		NETWORK SETUP
    serv_fd = socket(AF_INET_VER, SOCK_STREAM, 0);
	if(serv_fd == -1) LOG::LOG_ERR_NOTIFY("Unable to create socket! ", true);
    memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET_VER;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(SERV_PORT);

    if( bind(serv_fd, (const sockaddr*)&serv_addr, sizeof(serv_addr)) != 0)
		LOG::LOG_ERR_NOTIFY("Unable to bind serv socket! ", true);
	else LOG::LOG_NOTIFY("Port succesfully bound!");

	if (listen(serv_fd, 5) < 0) 
		LOG::LOG_ERR_NOTIFY("Unable to listen for incomming connections! ", true);
}

void Server::handle_connection(int conn_fd) {
	std::string msg;
	kickoff::Motion m;
	Conversation conv(conn_fd, false);
	conv.srv_init();
	bool kickoff_sent = false;

	//		SOCKET TIMEOUT
	struct timeval tv;
	tv.tv_sec = MSG_RECV_TIMEOUT_SEC;
	tv.tv_usec = MSG_RECV_TIMEOUT_MSEC;
	setsockopt(conn_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
	//vehicle.kickoff();
	while(!conv.is_down()) { //while we are connected to the user
		if (vehicle.swarm()) {
			if(!kickoff_sent) { //TODO: move this to slot
				kickoff::Kickoff k; 
				k.set_req_ok(true);
				conv.send(k.SerializeAsString(), kickoff::MsgType::KICKOFF);
				kickoff_sent = true;
			}
			auto [vel, acc, jerk] = vehicle.movement_data();
			m.set_velocity(vel);
			m.set_acceleration(acc);
			m.set_jerk(jerk);

            LOG::DEBUG("My motion: " + 
            std::to_string(vel) + ", " + 
            std::to_string(acc) + ", " +
            std::to_string(jerk) + ", ", DEBUG::TYPE_IP | DEBUG::LEVEL_ALL);

			conv.send(m.SerializeAsString(), kickoff::MsgType::MOTION);
			std::this_thread::sleep_for (std::chrono::milliseconds(WAIT_BETWEEN_MSG_MICS));
		} else { //if stopped or other
			conv.ping(); 
			std::this_thread::sleep_for (std::chrono::seconds(1));
			//vehicle.kickoff();
		}
	}
}

void Server::run() {
	start();
	LOG::DEBUG("Started Server!", DEBUG::LEVEL_BASIC | DEBUG::TYPE_IP);
    while(is_running()) {
		LOG::DEBUG("Waiting for incomming conenctions!", DEBUG::LEVEL_VERBOSE | DEBUG::TYPE_IP);

		int addrlen = sizeof(serv_addr), conn_fd;
		conn_fd = accept(serv_fd, (struct sockaddr *)&serv_addr, (socklen_t*)&addrlen);
        if(conn_fd > 0)
			LOG::LOG_NOTIFY("Accepted connection!");
		else LOG::LOG_ERR_NOTIFY("Unable to accept connection!", true);
		handle_connection(conn_fd);				
    }
}

Server::~Server() {
    if(serv_fd > 0) close(serv_fd);
}