## Jalopy Swarm Client

Part of my master's thesis "Synchronous vehicle moving system". It allows sharing information between queuing robots for the purpose of imitating actions.  
When paired together with ![Jalopy Swarm OS](https://gitlab.com/ppavacic/jalopy-swarm-os) allows creating a swarm  
of vehicles/robots that can do synchronous actions.

Possible uses, users include but are not limited to warehouse automation, automotive industry and tasks which require encrypted sharing of information between enqueued devices with low latency.

## Technical details

This project includes a new infrared protocol that was developed (Jalopy Swarm Communication Protocol) for the purpose of this project.  
The newly created protocol consists of two sub-protocols; infrared protocol, which I've named Jalopy NEC and internet protocol the Jalopy Network Protocol.

A general overview can be seen in the image below. Asymmetric encryption public keys are exchanged over infrared JNEC protocol. The symmetric key is then exchanged using asymmetric encryption. Symmetric key is used for further communication for lower latency and performance reasons where only two devices have that symmetric key at the same time. 

TCP/JNEC packets are used for control and management, while UDP packets are used to send movement information.

![Jalopy Communication Overview](https://gitlab.com/ppavacic/res/-/raw/main/Jalopy/jalopy-communication-overview.png)

### Setup

Libraries:

*   protobuf
*   openssl
*   cryptopp/crypto++

Tools:

*   ninja
*   pkg-config
*   meson
*   protoc

### Generating protobuf files

You might need to regenerate kickoff header and source file depending on your distribution and version of protobuf installed. From projects root directory, generate protobuf files using:

`protoc -I=./protobuf/conf --cpp_out=./protobuf/src/ ./protobuf/conf/kickoff.proto`

There are soft links in include/Protobuf/ and src/Protobuf/ that link to those files.

### Building

From projects root directory:  
`meson builddir && ninja -C builddir`

## Code comments

Check `./include/App/Constants.hpp.in` and `./include/App/meson.build` for possible pre-compile configurations for the project.

## Networking

All messages that are exchanged will be defined in `protobuf/conf/`. I've made it that way so that it is really easy to understand  
what kind of communication is there between entities without even looking at the code.
