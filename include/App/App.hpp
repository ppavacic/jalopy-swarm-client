#pragma once
#include <memory>
#include "Network/IRCommunicator.hpp"
#include "App/Constants.hpp"
#include "Vehicle/Vehicle.hpp"

class App {
public:
	App();
	~App();
	int run();
	bool running() { return running_t; };
	void set_running(bool running) { running_t = running; };
private:
	bool running_t = false;
	std::unique_ptr<Vehicle> vehicle;
	std::unique_ptr<IRCommunicator> ir;
	std::unique_ptr<Client> client;
	std::unique_ptr<Server> server;

};
