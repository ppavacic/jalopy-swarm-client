#pragma once
#include <fcntl.h>
#include <mutex>
#include <sys/mman.h>
#include "stdio.h"
#include "App/Log.hpp"

char* itoa(int num, char* buffer, int base);
void gpio_init();
void gpio_destroy();
bool gpio_state(int input_pin);

inline int strcmpb(const char str1[], const char str2[], int len) {
    for(int i = 0; i < len; i++) {
        if(str1[i] > str2[i]) return 1;
        else if(str1[i] < str2[i]) return -1;
    }
    return 0;
}

