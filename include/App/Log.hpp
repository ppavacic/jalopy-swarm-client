#pragma once
#include <string>
#include <string.h> //for strerrno
#include <chrono> //TODO for logging time
#include <fstream> //open file
#include <iostream> //output
#include <unistd.h>
#include <mutex>
#include "App/Constants.hpp"
#include "Log.hpp"
//TODO thread safety!!

/*!
 * Simple function to log stuff that we need in our project.
 
 function  | what it does
------------- | -------------
LOG_OUT		| write to logfile
LOG_ERR     | write to logerror file 
NOTIFY		| write to stdout
DEBUG 		| if in debug mode write to stdout
*/
 
class LOG {
public:
	LOG() = delete; 
	
	static void init() { //! don't forget to call init function at the start
		if(initialized) { 
			DEBUG("Calling LOG::init() twice!");
			return;
		}
		auto log_path = std::string(LOG_DIR) + "log-out-XX-XX-XX.log";
		auto err_path = std::string(LOG_DIR) + "log-err-XX-XX-XX.log";
		LOG::log_file.open(log_path);
		LOG::err_file.open(err_path);
		
		//LOG("Started logging\n out_fil: " << log_path << " err_file: " << err_path);
	}
	
	static void destroy() { //! call it at the end of program 
		if(log_file.is_open()) log_file.close(); 
		if(err_file.is_open()) err_file.close(); 
	}
	static inline void NOTIFY(const std::string& text) {
		std::lock_guard<std::mutex> out(stdout_mutex);
		std::cout << FORMAT_OUTPUT(text) << std::endl;
	}
	static inline void NOTIFY_ERR(const std::string& text) {
		std::lock_guard<std::mutex> out(stderr_mutex);
		std::cerr << FORMAT_OUTPUT(text) << std::endl;
	}
	static inline void LOG_NOTIFY(const std::string& text) {
		LOG_OUT(text); NOTIFY(text);
	}
	static inline void LOG_ERR_NOTIFY(const std::string& text, bool print_errno = false) {
		if(print_errno) {
			std::string text_with_errno =  text + ", C error: " + std::string(strerror(errno));
			LOG_ERR(text_with_errno); NOTIFY_ERR(text_with_errno);
		} else {LOG_ERR(text); NOTIFY_ERR(text);}

	}
	static inline void DEBUG(const std::string& text, const int debug_requirement = 0) {
		#if(DEBUG_MODE)
			const int show_debug_level = DEBUG_LEVEL & 0b0000000011111111;
			const int debug_level = debug_requirement & 0b0000000011111111;

			const int show_debug_type = DEBUG_LEVEL & 0b1111111100000000;
			const int debug_type = debug_requirement & 0b1111111100000000;

			std::lock_guard<std::mutex> out(stdout_mutex); //slow
			if((show_debug_type == debug_type || show_debug_type == 0 || debug_type == 0) && show_debug_level >= debug_level)
				std::cout <<  ": " << FORMAT_OUTPUT(text) << std::endl;
		#endif
	}

private:
	static inline std::mutex log_file_mutex, err_file_mutex, stdout_mutex, stderr_mutex;
	static inline void LOG_OUT(const std::string& text) {
		std::lock_guard<std::mutex> log(log_file_mutex);
		LOG::log_file << FORMAT_OUTPUT(text);
	}
	static inline void LOG_ERR(const std::string& text) {
		std::lock_guard<std::mutex> err(err_file_mutex);
		LOG::err_file << FORMAT_OUTPUT(text);
	}
	static inline std::string FORMAT_OUTPUT(const std::string& text) {
		return text;
	}
	static inline std::ofstream log_file, err_file;
	static inline bool initialized = false;	
};

