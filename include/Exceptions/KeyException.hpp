#pragma once
#include "Exceptions/JalopyException.hpp"

class KeyException : public JalopyException {
public:
    KeyException(
        const std::string msg, const std::string pp_filename, 
        const std::string func_name, int debug_level = DEBUG::TYPE_IP | DEBUG::LEVEL_BASIC
    ) : JalopyException(msg, pp_filename, func_name, debug_level, "KeyException") {};
};

