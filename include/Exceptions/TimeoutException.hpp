#pragma once
#include "Exceptions/JalopyException.hpp"

class TimeoutException : public JalopyException {
public:
    TimeoutException(
        const std::string msg, const std::string pp_filename, 
        const std::string func_name, int debug_level
    ) : JalopyException(msg, pp_filename, func_name, debug_level, "TimeoutException") {};
};

