/*! 
Other exceptions used in project are derived from this class.
*/
# pragma once
#include <exception>
#include <string>

class JalopyException : public std::exception {
public:
    JalopyException(const std::string msg, const std::string pp_filename, const std::string func_name, int debug_level = DEBUG::TYPE_SYSTEM | DEBUG::LEVEL_BASIC,
    std::string exception_name = "JalopyException")
    : msg(msg), pp(pp_filename), fun(func_name), debug_level(debug_level), 
    full_msg(exception_name + ": " + pp + ", " + fun + ", " + msg + "\n") {}
    
	const char * what () const throw () {
    	return full_msg.c_str();
    }
protected:
    const std::string msg, pp, fun;
    const int debug_level;
private:
    std::string full_msg;
};