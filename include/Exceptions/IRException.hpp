# pragma once
#include "JalopyException.hpp"
#include <string>

class IRException : public JalopyException {
public:
    IRException(
        const std::string msg, const std::string pp_filename, 
        const std::string func_name, int debug_level
    ) : JalopyException(msg, pp_filename, func_name, debug_level, "IRException") {};
};