#pragma once
#include <string>
#include <tuple>
#include <mutex>
#include <atomic>
#include "App/Constants.hpp"
#include "App/Log.hpp"
//! Class that holds information about vehicle on which this program is running
//TODO: make this class thread safe!

inline std::mutex his_ip_mutex, vehicle_state_mutex; //should implement one writer, multiple readers lock

class Vehicle {
public:
    Vehicle();
    bool stopped() const { 
        std::lock_guard<std::mutex> move_state_guard(vehicle_state_mutex);
        return move_state == STOPPED; 
    };
    bool swarm() const { 
        std::lock_guard<std::mutex> move_state_guard(vehicle_state_mutex);
        return move_state == SWARM; 
    };
    inline void kickoff();

    std::tuple<float, float, float> movement_data() { 
        return std::make_tuple(rand()%40, rand()%40, rand()%40); 
    };
    inline void set_swarm_speed(float velocity);
    void stop() { set_move_state(STOPPED); };
    void read_sensors() const {};
    std::string get_my_ip() const { return my_ip; };
    void update_my_ip(); // get my ip from interface
    std::string get_his_ip() const; // IP of vehicle in front
    bool his_ip_set() const;
    void set_his_ip(const std::string new_ip);
private:
    enum MoveState {UNDEFINED, STOPPED, DRIVING, SWARM};
    void set_move_state(MoveState state) { 
        std::lock_guard<std::mutex> move_state_guard(vehicle_state_mutex);
        move_state = state; 
    };
    //! swarm_t MoveState - car is currently being controlled by swarm mechanism
    MoveState move_state = STOPPED; //TODO: change to undefined_t
    float velocity_t = DEBUG_FLOAT, acceleration_t = DEBUG_FLOAT, jerk_t = DEBUG_FLOAT,
        desired_speed_t = DEBUG_FLOAT;
    std::string his_ip = "", my_ip = "";
    bool is_ipv6 = false;
};

void Vehicle::kickoff() {
    //start engine, do other stuff
    LOG::DEBUG("Started kickoff!", DEBUG::LEVEL_BASIC);
    set_move_state(SWARM);
}

void Vehicle::set_swarm_speed(float velocity) {
    //TODO: add e because often it wont be = 0
    //! swarm can only go forwards, thats why there is abs
    if(velocity > 0) desired_speed_t = velocity; 
    else if(velocity < 0); //TODO stop swarm;
    else ;//TODO velocity 0
}