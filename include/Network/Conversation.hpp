#pragma once
#include <sys/socket.h>
#include <memory>
#include <sstream>
#include <unistd.h>
#if(ARM) //TODO: test
	#include <cryptopp/rsa_armv4.h>
#else
	#include <cryptopp/rsa.h>
#endif
#include "Network/Encryptor.hpp"
#include "Network/Common.hpp"
#include "App/Constants.hpp"

//! initializes conversation with other party, 
class Conversation {
    public:
        Conversation(int soc_fd, bool im_client) : 
            soc_fd(soc_fd), im_client(im_client), key(0x00, AES_KEY_LEN), iv(AES::BLOCKSIZE),
            cfb_encryption(key, key.size(), iv), cfb_decryption(key, key.size(), iv)
             {
                if(!im_client) {
                    //Conversation::rnd.GenerateBlock(key, sizeof(key));
	                //Conversation::rnd.GenerateBlock(iv, sizeof(iv));
                }
                memset(key, 0x00, key.size());
                memset(iv, 0x00, iv.size());

                //rnd.GenerateBlock( key, key.size() );
                //rnd.GenerateBlock( iv, iv.size() );
                //cfb_decryption.SetKeyWithIV(key, key.size(), iv); // OVDJE JE PROBLEM
                //cfb_encryption.SetKeyWithIV(key, key.size(), iv);
               
            };
        ~Conversation() { close(); };
        Conversation() = delete;
        Conversation(const Conversation&) = delete; // Conversation shouldn't be copied, because of network fragility 
        Conversation& operator=(const Conversation&) = delete;
        
        inline void send(const std::string msg, kickoff::MsgType type);
        inline std::tuple<kickoff::MsgType, std::string> receive();
        inline void ping();
        inline void close();
        void srv_init();
        void cli_init();
        inline bool is_up() { return state == UP; };
        inline bool is_down() { return state == DOWN; };
    private:
        enum STATE{DOWN, INIT, CONNECTING, ENCRYPTION_CHECK, UP, CLOSING, ERR};
        enum STATE state = DOWN;
        const int soc_fd = 0;
        const bool im_client;
        void set_state(STATE new_state) {
            LOG::DEBUG("Conversation state changed from " + 
            std::to_string(state) + " to " + std::to_string(new_state), 
            DEBUG::TYPE_IP | DEBUG::LEVEL_VERBOSE);
            state = new_state;
        };

        //encryption stuff
        SecByteBlock key, iv;
        static CryptoPP::AutoSeededRandomPool rnd;
        std::string aes_key;
        CFB_Mode<AES>::Encryption cfb_encryption;
        CFB_Mode<AES>::Decryption cfb_decryption;
};

void Conversation::send(const std::string msg, kickoff::MsgType type) {
    //      ENCRYPT AND CLASSIFY
    std::ostringstream oss;  
    unsigned int total_len = msg.length() +1; //1byte for type
    byte msg_enc[total_len];
    msg_enc[0] = type;
    memcpy(&msg_enc[1], msg.data(), total_len);

    CFB_Mode<AES>::Encryption cfbEncryption(key, key.size(), iv);
    cfbEncryption.ProcessData(msg_enc, (byte *)msg_enc, total_len); 
	//cfb_encryption.ProcessData(msg_enc, msg_enc, sizeof(msg_enc));// this doesn't work when on heap or when using  .SetKeyWithIV
    oss << msg_enc;

    //      SEND
    int status = Network::send_packet(oss.str(), soc_fd);
    if(status > 0 && !is_up()) set_state(UP);
    else if(status == 0) close(); 
    else if(status < 0) {
        set_state(ERR);
        close();
    }
}

std::tuple<kickoff::MsgType, std::string> Conversation::receive() {
    char msg_enc[MAX_MSG_LEN];
    std::ostringstream oss;
    kickoff::MsgType type;
    //      RECEIVE
    int status = Network::receive_packet(msg_enc, soc_fd);
    if(status < 0) state = ERR;
    else if(status == 0) close();
    //      DECRYPT AND CLASSIFY
    byte msg[MAX_MSG_LEN];
    CFB_Mode<AES>::Decryption cfbDecryption(key, key.size(), iv);
    cfbDecryption.ProcessData(msg, (byte *)msg_enc, status);
    type = (kickoff::MsgType) msg[0]; //TODO check if this works
    oss << &msg[1];
    //printf("Last char is: %c, Num of chars: %d\n", msg[status-1], status);
    std::string str2((char*) &msg[1], status-1); //first byte for type
    return std::make_tuple(type, str2);
}

void Conversation::ping() {
    send("", kickoff::MsgType::PING);
}

void Conversation::close() {
    set_state(CLOSING);
    ::close(soc_fd); //TODO: error handling
    set_state(DOWN);
}