#pragma once
#include <sys/socket.h>
#include <string>
#include <cmath>
#include "Protobuf/kickoff.pb.h"
#include "App/Constants.hpp"
#include "App/Log.hpp"

namespace Network {
    // returns length of message sent, or -1 if message was not sent succesfully
    inline int send_packet(const std::string& msg, int soc_fd) { 
        if(msg.length() > MAX_MSG_LEN) LOG::DEBUG("msg length > MAX_MSG_LEN", 2);
        int status = send(soc_fd, msg.data(), msg.length(), 0); //check MSG_NOSIGNAL
        if(status == -1) LOG::LOG_ERR_NOTIFY("Unable to send packet", true);
	    return status;
    }

    //first byte is used to indicate type
    inline int receive_packet(char msg_recv[], int soc_fd) {
        int status = recv(soc_fd, msg_recv, MAX_MSG_LEN, 0);
        if(status < 0) LOG::LOG_ERR_NOTIFY("Unable to receive packet", true);
        return status;
    }

    inline in_addr_t ip_text_to_binary(const std::string ip_text) {
        in_addr_t addr;
        inet_pton(AF_INET_VER, ip_text.data(), &addr);
        return addr;
    }
}