#pragma once
#include <mutex>
#include "Vehicle/Vehicle.hpp"
#include "Exceptions/IRException.hpp"

inline std::mutex access_to_pins;

class IRCommunicator  {
public:
    IRCommunicator(Vehicle& vehicle);
    ~IRCommunicator();
    void run();

    void start() { set_receiving(true); set_transmitting(true); set_running(true); };
    void stop(const std::string& request_function_name) { 
        LOG::DEBUG(
            "IR communicator stopped by: " + request_function_name, 
            DEBUG::LEVEL_BASIC | DEBUG::TYPE_IR
        );
        m_running = false; 
    };
    bool is_running() { return m_running; }; 
    #if(DEBUG_MODE)  //functions used in some tests in DEBUG mode
        void transmit_my_ip(); 
        std::string receive();
        void receive_(char received_binary[IR_PACKET_BITS+1]);
    #endif
private:
    enum STATE {WAIT_HELLO, RECV_DATA}; 
    #if!(DEBUG_MODE)
        void transmit_my_ip();
        std::string receive();
    #endif
    void listen_for_ip(char received_binary[IR_PACKET_BITS+1]);

    Vehicle& vehicle;
    std::string binary_ip_to_string(const char binary_ip[], int size, int size_per_part);
    char ip_binary_string[IR_PACKET_BITS+1]; //string of binary numbers made for sending IP
    bool m_receiving = false;
    bool m_transmitting = false;
    bool m_running = false;
    bool receiving() { return m_receiving; };
    bool transmitting() { return m_transmitting; }

    void set_receiving(bool new_state) { m_receiving = new_state; };
    void set_transmitting(bool new_state) { m_transmitting = new_state; };
    void set_running(bool new_state) { m_running = new_state; };
};