#pragma once
#include <string>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include "App/Log.hpp"
#include "App/Constants.hpp"
#include "Network/Common.hpp"
/*!
    This class isn't in use anymore, but could later be useful if need for 
    central certificate authority appears.
*/

class Lookup {
public:
    Lookup(const std::string lookup_ip) {
        memset(&lookup_addr, 0, sizeof(lookup_addr));
        lookup_addr.sin_family = AF_INET_VER;
        lookup_addr.sin_addr.s_addr = INADDR_ANY;//Network::ip_text_to_binary(LOOKUP_IP);
        lookup_addr.sin_port = htons(LOOKUP_PORT);

        tv.tv_sec = MSG_RECV_TIMEOUT_SEC;
        tv.tv_usec = MSG_RECV_TIMEOUT_MSEC;
    };

    std::string ask(const std::string target_plate) {
        int result = 0;
        char in_msg[MAX_MSG_LEN*5];
        lookup_fd = socket(AF_INET_VER, SOCK_STREAM, 0);
        setsockopt(lookup_fd, SOL_SOCKET, SO_RCVTIMEO | SO_REUSEADDR, (const char*)&tv, sizeof tv);

        if(connect(lookup_fd, (struct sockaddr *) &lookup_addr, sizeof(lookup_addr)) == 0) {
			LOG::DEBUG("Succesfully connected to lookup!");
            result = send(lookup_fd, target_plate.data(), target_plate.length(), 0);
            if(result < 0) LOG::LOG_ERR_NOTIFY("Sending msg to lookup error!", true);

            result = Network::receive_packet(in_msg, lookup_fd);
            if(result < 0) LOG::LOG_ERR_NOTIFY("Receiving msg to lookup error!", true);
            close(lookup_fd);
		} else LOG::LOG_ERR_NOTIFY("Unable to connect to lookup!", true); //TODO add network exception here
        
        return std::string(in_msg, result);
    }
private:
    struct sockaddr_in lookup_addr;
    struct timeval tv;
    int lookup_fd;
};
