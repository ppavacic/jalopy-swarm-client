#pragma once
#include <cryptopp/gcm.h>
#include <cryptopp/aes.h>
#include <cryptopp/osrng.h>
#include <cryptopp/rsa.h>
#include <sstream>
#include <iostream>
#if(ARM) //TODO: test
	#include <cryptopp/rsa_armv4.h>
#else
	#include <cryptopp/rsa.h>
#endif
#include "App/Log.hpp"
#include "Exceptions/KeyException.hpp"
using namespace CryptoPP;
#define RSA_KEY_LEN 256 //2048 bit RSA, max word encryption 32B
#define AES_KEY_LEN 16
#define AES_BLOCK_SIZE 16
#define PP_FILE "Encryptor.hpp"

class Encryptor {
public:
    inline Encryptor();

	//AES encryption
	inline std::string aes_encrypt(const std::string msg, const int key_ind);
	inline std::string aes_decrypt(const std::string msg_dec, const int key_ind);
	inline int aes_add_key(); //returns key index of newly generated key
	inline void aes_remove_key(const int key_ind);

	//RSA encryption
    inline std::string rsa_encrypt(const std::string msg); //! encrypt using passed public key
	inline std::string rsa_decrypt(const std::string msg_enc); //! decrypt using my private key
private:
    //! contains key and iv for different keys and whether it is in use. Last field is used for lazy delete
    std::vector<std::tuple<SecByteBlock, SecByteBlock, bool>> aes_key;

	CryptoPP::RSA::PrivateKey priv_key; //! my private key and my public key
	CryptoPP::RSA::PublicKey pub_key;
	CryptoPP::AutoSeededRandomPool rnd;

};

Encryptor::Encryptor() {
	priv_key.GenerateRandomWithKeySize(rnd, RSA_KEY_LEN);
	pub_key.AssignFrom(priv_key);

	// 1 key vehicle in front, 1 for vehicle in back, 2 in case i want to expand communication
	aes_key.reserve(4); 
}

std::string Encryptor::aes_encrypt(const std::string msg, const int key_ind) {
	byte enc_msg[msg.length()];
	std::ostringstream oss;

	if(key_ind > (int) aes_key.size()) throw KeyException("AES key_ind out of bounds!", PP_FILE, "aes_encrypt");
	auto& [key, iv, exists] = aes_key[key_ind];
	if(!exists) throw KeyException("AES key marked as deleted!", PP_FILE, "aes_encrypt");

	CFB_Mode<AES>::Encryption cfbEncryption(key, key.size(), iv); //TODO try to move this to heap
	cfbEncryption.ProcessData(enc_msg, (byte *)msg.data(), msg.length()); 
	oss << enc_msg;
	return oss.str();
}

std::string Encryptor::aes_decrypt(const std::string msg_enc, const int key_ind) {
	byte msg[msg_enc.length()];
	std::ostringstream oss;

	if(key_ind > (int) aes_key.size()) throw KeyException("AES key_ind out of bounds!", PP_FILE, "aes_decrypt");
    auto& [key, iv, exists] = aes_key[key_ind];
	if(!exists) throw KeyException("AES key marked as deleted!", PP_FILE, "aes_decrypt");
	
	CFB_Mode<AES>::Decryption cfbDecryption(key, key.size(), iv);
	cfbDecryption.ProcessData(msg, (byte *)msg_enc.data(), msg_enc.length());
	
	oss << msg;
	return oss.str();
}

int Encryptor::aes_add_key() {
    // Generate a random key
    SecByteBlock key(0x00, AES_KEY_LEN), iv(AES::BLOCKSIZE);
	rnd.GenerateBlock(key, sizeof(key));
	rnd.GenerateBlock(iv, sizeof(iv));
    aes_key.push_back(std::make_tuple(key, iv, true));
	LOG::DEBUG("AES key added!");
	return aes_key.size() -1; //return index of newly created key
}

void Encryptor::aes_remove_key(const int key_ind) {
	if(key_ind > (int) aes_key.size()) throw KeyException("AES key_ind out of bounds!", PP_FILE, "aes_remove_key");
    auto& [key, iv, exists] = aes_key[key_ind];
	if(!exists) throw KeyException("AES key marked as deleted!", PP_FILE, "aes_remove_key");
	exists = false;
	LOG::DEBUG("AES key removed!");
}

std::string Encryptor::rsa_encrypt(const std::string msg) {
	Integer msg_byte = Integer((const byte *) msg.data(), msg.size());
	Integer msg_enc = pub_key.ApplyFunction(msg_byte);
	std::ostringstream oss;
	oss << msg_enc;
	return oss.str();
}

std::string Encryptor::rsa_decrypt(const std::string msg_enc) {
	Integer msg_enc2 = Integer(msg_enc.data());
	Integer msg_int = priv_key.CalculateInverse(rnd, msg_enc2);
	std::string msg;
	msg.resize(msg_int.MinEncodedSize()+1); //TODO: i think +1 because of \0 to avoid double resize
	msg_int.Encode((byte *)msg.data(), msg.size());
	return msg;
}

#undef PP_FILE
