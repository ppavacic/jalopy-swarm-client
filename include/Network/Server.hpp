#pragma once
#include <sys/socket.h>
#include <netinet/in.h>
#include "Network/Lookup.hpp"
#include "Vehicle/Vehicle.hpp"
/*! 
    Server that listens for incomming connections from Clients (other vehicles).
    After connection is established this class sends kickoff::Kickoff message to client
    when this vehicle starts up. After that message kickoff::Motion is sent every n msec
    for couple of seconds before ending conenction with Client.
*/

class Server {
public:
    Server() = delete;
    Server(Vehicle &vehicle);
    ~Server();
    void run();
    void start() { running = true; };
    void stop() { running = false; };
    bool is_running() { return running; };
private:
    void handle_connection(int conn_fd);
    Vehicle &vehicle;
    Lookup lookup;
    struct sockaddr_in serv_addr;
    int serv_fd;
    bool running = false;
};