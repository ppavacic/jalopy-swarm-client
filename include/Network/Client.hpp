#pragma once
#include <sys/socket.h>
#include <netinet/in.h>
#include "Vehicle/Vehicle.hpp"
#include "Network/Conversation.hpp"
#include "Network/Lookup.hpp"
#include "Protobuf/kickoff.pb.h"

//! base class for handling Client connection.
class Client {
public:
	Client() = delete;
	Client(Vehicle &vehicle);
	~Client();
	void operator=(Client const&) = delete; // we shouldn't copy Client instances
	void run();
	bool running() { return running_t; };
	void set_running(bool running) { 
		LOG::DEBUG("Started Client!", DEBUG::TYPE_IP | DEBUG::LEVEL_BASIC);
		running_t = running; 
	};
	void handle_connection();
private:
	void setup_socket();
	Vehicle &vehicle;
	Lookup lookup;
	int serv_fd;
	struct sockaddr_in serv_addr;
	bool running_t = false;
	struct timeval tv;
};
