#pragma once
#include <thread>
#include "Network/Client.hpp"
#include "Network/Server.hpp"
#include "Network/Encryptor.hpp"
#include "Network/Conversation.hpp"
#include "Network/IRCommunicator.hpp"
#include "Network/irslinger.h"
#include "Protobuf/kickoff.pb.h"

#if(DEBUG_MODE)

void encryption_test() {
	Encryptor e;
	//AES
	int k = e.aes_add_key();
	std::string enc_aes = e.aes_encrypt("Hello! Workkk", k);
	std::string dec_aes = e.aes_decrypt(enc_aes, k);
	std::cout << "enc aes: " << enc_aes << std::endl << "dec aes: " << dec_aes << std::endl;
	//RSA:
	std::string enc = e.rsa_encrypt("12345678123456781234567812345678");
	std::string dec = e.rsa_decrypt(enc);
	std::cout << "enc: " << std::hex << enc << ", dec: " << std::dec << dec << std::endl;
}

void server_test() {
	Vehicle v;
	Server s(v);
	s.run();
}

void client_test() {
	Vehicle v;
	Client c(v);
	c.run();
}

void ir_test() {
	Vehicle v;
	IRCommunicator ir_comm(v);
	ir_comm.run();
	std::cout << v.get_his_ip();
}

void ir_receive() {
	try {
		Vehicle v;
		IRCommunicator ir_comm(v);
		std::cout << "Starting receive test" << std::endl;
		ir_comm.receive();
	} catch (IRException& e) {
		std::cout << e.what() << std::endl;
	}
	std::cout << "Ir receive closing" << std::endl;
}

void ir_transmit() {
	LOG::DEBUG("ir_transmit: starting IR transmission", DEBUG::LEVEL_BASIC | DEBUG::TYPE_IR);
	Vehicle v;
    IRCommunicator irComm(v);
	while(true) {
		irComm.transmit_my_ip();
		LOG::DEBUG("ir_transmit: Sent IR packet!", DEBUG::LEVEL_VERBOSE | DEBUG::TYPE_IR);
		std::this_thread::sleep_for(std::chrono::milliseconds(IR_THREAD_SLEEP_MS));
	}
}

void ir_receive_and_client() {
	Vehicle v;
	Client client(v);
	IRCommunicator ir(v);
	LOG::DEBUG("Starting IR receive and client test", DEBUG::LEVEL_ALL);
	std::thread ir_thr([=, &ir] {ir.receive();});
	std::thread client_thr([=, &client] {client.run();});
	client_thr.join();
	ir_thr.join();
}

void ir_transmit_and_server() {
	Vehicle v;
	Server server(v);
	IRCommunicator ir(v);
	LOG::DEBUG("Starting IR receive and server test", DEBUG::LEVEL_ALL);
	std::thread ir_thr([=, &ir] {
		while(1) {
			ir.transmit_my_ip();
			LOG::DEBUG("ir_transmit: Sent IR packet!", DEBUG::LEVEL_VERBOSE | DEBUG::TYPE_IR);
			std::this_thread::sleep_for(std::chrono::milliseconds(IR_THREAD_SLEEP_MS));
		}

	});
	std::thread server_thr([=, &server] {server.run();});
	server_thr.join();
	ir_thr.join();
}

void test(int argc, char* argv[]) {
	if(argc == 2) {
		std::cout << "Specify test" << std::endl;
		return;
	}

	if(!strcmp(argv[1], "server")) server_test();
	else if (!strcmp(argv[2], "client")) client_test();
	else if (!strcmp(argv[2], "ir-test")) ir_test();
	else if (!strcmp(argv[2], "ir-transmit")) ir_transmit();
	else if (!strcmp(argv[2], "ir-receive")) ir_receive();
	else if (!strcmp(argv[2], "ir-receive+client")) ir_receive_and_client();
	else if (!strcmp(argv[2], "ir-transmit+server")) ir_transmit_and_server();

	else std::cout << "Unknown test" << std::endl;
}

#endif